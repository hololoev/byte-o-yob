# Converting integer to byte string/array


## Installation

```
npm i --save byte-o-yob
```

## Usage

```
const yoba = require('byte-o-yob');

let bytesString = yoba.uint_16_t_to_bytes_s(12345);
```

## Functions


### uint_16_t_to_bytes_s(int)

Converts short int to 2 byte string


### uint_16_t_to_bytes_a(int)

Converts short int to 2 byte array of int


### uint_32_t_to_bytes_s(int)

Converts int to 4 byte string


### uint_32_t_to_bytes_a(int)

Converts int to 4 byte array of int


### uint_from_s(str)

Converts any string(only byte char allowed) to integer


### uint_from_a(array of bytes)

Converts array of bytes to integer


### hexDecode(str)

Converts hex decoded string to common string


### hexEncode(str)

Converts common string to hex encoded


### string_dump(str)

Returns array of hex bytes


## Features

- Doesn't work with long int.
- No external dependencies
