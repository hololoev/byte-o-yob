
const assert = require('assert').strict;

const testModule = require('../index');

function randomRange(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

describe('Check the fucking out', function() {
  
  it('uint_16_t_to_bytes_s', function() {
    
    for(let i=0; i<100; i++) {
      let b1 = randomRange(0, 255);
      let b2 = randomRange(0, 255);
      let tstVal = (b2 << 8) + b1;    
      let resVal = testModule.uint_16_t_to_bytes_s(tstVal);
      
      assert.equal(resVal.charCodeAt(0), b2);
      assert.equal(resVal.charCodeAt(1), b1);
    }
    
  });
  
  it('uint_16_t_to_bytes_a', function() {
    
    for(let i=0; i<100; i++) {
      let b1 = randomRange(0, 255);
      let b2 = randomRange(0, 255);
      let tstVal = (b2 << 8) + b1;    
      let resVal = testModule.uint_16_t_to_bytes_a(tstVal);

      assert.equal(resVal[ 0 ], b2);
      assert.equal(resVal[ 1 ], b1);
    }
    
  });
  
  it('uint_32_t_to_bytes_s', function() {
    
    for(let i=0; i<1000; i++) {
      let b1 = randomRange(0, 255);
      let b2 = randomRange(0, 255);
      let b3 = randomRange(0, 255);
      let b4 = randomRange(0, 127); // if > 127 then will be negotive
      
      let tstVal = (b4 << 24) + (b3 << 16) + (b2 << 8) + b1;    
      let resVal = testModule.uint_32_t_to_bytes_s(tstVal);

      assert.equal(resVal.charCodeAt(0), b4);
      assert.equal(resVal.charCodeAt(1), b3);
      assert.equal(resVal.charCodeAt(2), b2);
      assert.equal(resVal.charCodeAt(3), b1);
    }
    
  });
  
  it('uint_32_t_to_bytes_a', function() {
    
    for(let i=0; i<1000; i++) {
      let b1 = randomRange(0, 255);
      let b2 = randomRange(0, 255);
      let b3 = randomRange(0, 255);
      let b4 = randomRange(0, 127); // if > 127 then will be negotive
      
      let tstVal = (b4 << 24) + (b3 << 16) + (b2 << 8) + b1;    
      let resVal = testModule.uint_32_t_to_bytes_a(tstVal);

      assert.equal(resVal[ 0 ], b4);
      assert.equal(resVal[ 1 ], b3);
      assert.equal(resVal[ 2 ], b2);
      assert.equal(resVal[ 3 ], b1);
    }
    
  });
  
  it('uint_from_s 2 bytes', function() {
    
    for(let i=0; i<1000; i++) {
      let tstVal = '' + String.fromCharCode(randomRange(0, 255)) + String.fromCharCode(randomRange(0, 255));
      
      let b1 = tstVal.charCodeAt(0).toString(16);
      let b2 = tstVal.charCodeAt(1).toString(16);
      
      if( b1.length < 2)
        b1 = '0' + b1; // add lead 0
      if( b2.length < 2)
        b2 = '0' + b2; // add lead 0
      
      let strVal = b1 + b2;
      let resVal = testModule.uint_from_s(tstVal);
      let resStrVal = resVal.toString(16).padStart(4, '0');
      
      assert.equal(strVal, resStrVal);
    }
    
  });
  
  it('uint_from_s 4 bytes', function() {
    
    for(let i=0; i<100000; i++) {
      let tstVal = '' + String.fromCharCode(randomRange(0, 127)) + String.fromCharCode(randomRange(0, 255)) + String.fromCharCode(randomRange(0, 255)) + String.fromCharCode(randomRange(0, 255));
      
      let b1 = tstVal.charCodeAt(0).toString(16);
      let b2 = tstVal.charCodeAt(1).toString(16);
      let b3 = tstVal.charCodeAt(2).toString(16);
      let b4 = tstVal.charCodeAt(3).toString(16);
      
      if( b1.length < 2)
        b1 = '0' + b1; // add lead 0
      if( b2.length < 2)
        b2 = '0' + b2; // add lead 0
      if( b3.length < 2)
        b3 = '0' + b3; // add lead 0
      if( b4.length < 2)
        b4 = '0' + b4; // add lead 0
      
      let strVal = b1 + b2 + b3 + b4;
      let resVal = testModule.uint_from_s(tstVal);
      let resStrVal = resVal.toString(16).padStart(8, '0');
      
      for(let i=0; i<(8-resStrVal.length); i++)
        resStrVal = '0' + resStrVal;

      assert.equal(strVal, resStrVal);
    }
    
  });
  
  it('uint_from_a 2 bytes', function() {
    
    for(let i=0; i<1000; i++) {
      let tstVal = [ randomRange(0, 255), randomRange(0, 255) ];
      
      let b1 = tstVal[ 0 ].toString(16);
      let b2 = tstVal[ 1 ].toString(16);
      
      if( b1.length < 2)
        b1 = '0' + b1; // add lead 0
      if( b2.length < 2)
        b2 = '0' + b2; // add lead 0
      
      let strVal = b1 + b2;
      let resVal = testModule.uint_from_a(tstVal);
      let resStrVal = resVal.toString(16).padStart(4, '0');
      
      assert.equal(strVal, resStrVal);
    }
    
  });
  
  it('uint_from_a 4 bytes', function() {
    
    for(let i=0; i<100000; i++) {
      let tstVal = [ randomRange(0, 127), randomRange(0, 255), randomRange(0, 255), randomRange(0, 255) ];
      
      let b1 = tstVal[ 0 ].toString(16);
      let b2 = tstVal[ 1 ].toString(16);
      let b3 = tstVal[ 2 ].toString(16);
      let b4 = tstVal[ 3 ].toString(16);
      
      if( b1.length < 2)
        b1 = '0' + b1; // add lead 0
      if( b2.length < 2)
        b2 = '0' + b2; // add lead 0
      if( b3.length < 2)
        b3 = '0' + b3; // add lead 0
      if( b4.length < 2)
        b4 = '0' + b4; // add lead 0
        
      let strVal = b1 + b2 + b3 + b4;
      let resVal = testModule.uint_from_a(tstVal);
      let resStrVal = resVal.toString(16).padStart(8, '0');
      
      assert.equal(strVal, resStrVal);
    }
    
  });
  
  it('hexEncode', function() {
    const testString = '01234567890qwertyuiop[]asdfghjkl;zxcvbnm,./~!@#$%^&*()_+йцукенгшщзхъфывпролджэячсмитьбю.ё';
    const testResult = '303132333435363738393071776572747975696f705b5d6173646667686a6b6c3b7a786376626e6d2c2e2f7e21402324255e262a28295f2b43944644343a43543d43344844943744544a44444b43243f44043e43b43443644d44f44744143c43844244c43144e2e451';
    
    assert.equal(testResult, testModule.hexEncode(testString));
  });
  
  it('hexDecode with wrong string', function() {
    try {
      testModule.hexDecode('111');
    } catch(err) {
      if ( err.toString() == 'Error: Wrong input string length' )
        return true;
    }
    
    throw new Error();
  });
  
  it('hexDecode', function() {
    const testResult = 'qwertyuiop[]';
    const testString = '71776572747975696f705b5d';

    assert.equal(testResult, testModule.hexDecode(testString));
  });
  
}); 
