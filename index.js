
function string_dump(str) {
  let dump = [];
  for(let i=0; i<str.length; i ++)
    dump.push(str.charCodeAt(i).toString(16).padStart(2, '0'));
  
  return dump;
}

function hexDecode(str) {
  if( str.length % 2 ) {
    throw new Error('Wrong input string length');
  }
  
  let result = '';
  
  for(let i=0; i<(str.length/2); i++) {
    let lBits = str.charCodeAt(i * 2);
    let rBits = str.charCodeAt(i * 2 + 1);
    
    if( lBits < 58 )
      lBits -= 48;
    else
      lBits -= 87;
    
    if( rBits < 58 )
      rBits -= 48;
    else
      rBits -= 87;
    
    result += String.fromCharCode((lBits << 4) | rBits);
  }
  
  return result;
}

function hexEncode(str) {
  let result = '';
  
  for (let i=0; i<str.length; i++)
    result += str.charCodeAt(i).toString(16).padStart(2, '0');
    
  return result;
}

function uint_16_t_to_bytes_s(int) {
  let b1 =  int & 0x000000ff;
  let b2 = (int & 0x0000ff00) >> 8;

  return '' + String.fromCharCode(b2) + String.fromCharCode(b1);
}

function uint_16_t_to_bytes_a(int) {
  let b1 =  int & 0x000000ff;
  let b2 = (int & 0x0000ff00) >> 8;

  return [ b2, b1 ];
}

function uint_32_t_to_bytes_s(int) {
  let b1 =  int & 0x000000ff;
  let b2 = (int & 0x0000ff00) >> 8;
  let b3 = (int & 0x00ff0000) >> 16;
  let b4 = (int & 0xff000000) >> 24;

  return '' + String.fromCharCode(b4) + String.fromCharCode(b3) + String.fromCharCode(b2) + String.fromCharCode(b1);
}

function uint_32_t_to_bytes_a(int) {
  let b1 =  int & 0x000000ff;
  let b2 = (int & 0x0000ff00) >> 8;
  let b3 = (int & 0x00ff0000) >> 16;
  let b4 = (int & 0xff000000) >> 24;

  return [ b4, b3, b2, b1 ];
}

function uint_from_s(src) {
  let result = 0;
  for(let i=0; i<src.length; i++)
    result += src.charCodeAt(i) << ((src.length-i-1)*8);
  
  return result;
}

function uint_from_a(src) {
  let result = 0;
  for(let i=0; i<src.length; i++)
    result += src[ i ] << ((src.length-i-1)*8);
  
  return result;
}

module.exports = {
  uint_16_t_to_bytes_s,
  uint_16_t_to_bytes_a,
  uint_32_t_to_bytes_s,
  uint_32_t_to_bytes_a,
  uint_from_s,
  uint_from_a,
  string_dump,
  hexEncode,
  hexDecode
};
